var gulp = require('gulp'),
	connect = require('gulp-connect')
	historyApiFallback = require('connect-history-api-fallback')
	jshint =  require('gulp-jshint')
	inject = require('gulp-inject');

// servidor
gulp.task('server', function () {
	connect.server({
		name: 'Codea DEV',
		hostname: '192.168.1.66',
		root: ['./app', './assets'],
		port:  8000,
		livereload: true,
		history: console.log('hoala futuro'),
		middleware: function(connect, opt) {
	      return [ historyApiFallback ({})];
	    }

	});
});

// buscar todos mis archivos que necesito checar
gulp.task('filesReload', function () {
	gulp.src(['./app/*.html', './app/*.js'])
    	.pipe(connect.reload());
});

// los vijilo si hay cambios
gulp.task('watch', function () {
	gulp.watch(['./app/**/*.html', './app/assets/css/**.css', './app/**/*.controller.js'], ['filesReload']);
});

// pruebas al codigo JS
gulp.task('test', function() {

	return gulp.src('./app/login/**/*.js')
	    .pipe(jshint())
	    .pipe(jshint.reporter('default', { verbose: true, indent: 4 }));

});

// ijetctar cjs y css
gulp.task('index', function () {
	gulp.src('./app/index.html')
  	.pipe(inject(gulp.src(['./app/**/*.controller.js', './app/assets/css/*.css'], {read: false}), {relative: true}))
  	.pipe(gulp.dest('./app'));

});

gulp.task('default', ['server', 'watch', 'index']);