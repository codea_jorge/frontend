(function() {
    'use strict';

    angular
        .module('app')
        .config(config);

    function config($logProvider, $urlRouterProvider, $httpProvider ,$stateProvider, $mdIconProvider, 
                    $mdThemingProvider, $localStorageProvider){

        // configurar el nombre del localStorage como prefijo    
        $localStorageProvider.setKeyPrefix('localStorage_');

        //para mostrar los long solo cuando estamos trabajando local mente
        $logProvider.debugEnabled(true);

        // tema por defecto
        $mdThemingProvider.theme('default', {
            
        })
            //.backgroundPalette('wite')
            .primaryPalette('blue-grey', {
                'default': '400',
                'hue-1': '50', 
                'hue-2': '100',
                'hue-3': '800',
            })
            .accentPalette('green', {
                'default' : '500',
                'hue-1': 'A400', 
                'hue-2': 'A700',
                'hue-3': 'A100' 
            })
            .warnPalette('red');

        // tema django
        $mdThemingProvider.theme('django', {

        })
            .backgroundPalette('blue')
            .primaryPalette('blue', {
                'default': '400',
                'hue-1': '50', 
                'hue-2': '100',
                'hue-3': '800',
            })
            .accentPalette('teal', {
                'default' : '500',
                'hue-1': 'A400', 
                'hue-2': 'A700',
                'hue-3': 'A100' 
            })
            .warnPalette('red');

        

        $mdIconProvider.fontSet('md', 'material-icons');

        // Color para el navegador
        $mdThemingProvider.enableBrowserColor({

        });

        // temas dinamicos
        $mdThemingProvider.alwaysWatchTheme(true);

        $urlRouterProvider.otherwise('/');


        $stateProvider
            .state('login', { 
                url: '/',
                templateUrl: 'login/views/login.tpl.html',
                controller: 'loginController',
                controllerAs: 'ctrlLogin'    
            })
            .state('dashboard', {
                url: '/dashboard',
                abstract: true,
                templateUrl: 'dashboard/views/main.tpl.html',
                controller: 'mainController',
                controllerAs: 'ctrlMain'
            })
            .state('dashboard.notas', {
                url: '/notas',
                templateUrl: 'notas/views/notas.tpl.html',
                controller: 'notasController',
                controllerAs: 'ctrlNotas'
            })
            .state('dashboard.users', {
                url: '/users',
                templateUrl: 'users/views/users.tpl.html',
            })
            .state('dashboard.users.profile', {
                url: '/profile',
                templateUrl: 'users/views/profile.tpl.html',
            })
        
    }

})();