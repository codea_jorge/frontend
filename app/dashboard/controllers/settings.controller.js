(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('settingsController', settingsController);


    function settingsController($rootScope, $log, $mdDialog, $localStorage, Constants) {

        var vm = this;
        $log.debug('Controlador de settings');

        $log.debug('constantes', Constants.Themes);

        vm.cancel = cancel;
        vm.showEdit = showEdit;

        function cancel() {
	    	$mdDialog.cancel();
	    };

        function showEdit(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.prompt()
      .title('What would you name your dog?')
      .textContent('Bowser is a common name.')
      .placeholder('Dog name')
      .ariaLabel('Dog name')
      .initialValue('Buddy')
      .targetEvent(ev)
      .ok('Okay!')
      .cancel('I\'m a cat person');

    $mdDialog.show(confirm).then(function(result) {
      $scope.status = 'You decided to name your dog ' + result + '.';
    }, function() {
      $scope.status = 'You didn\'t name your dog.';
    });
          };

        // funciones (metodos)
        //vm.selectTheme = selectTheme; 
        //vm.defaultTheme = defaultTheme;

        // localStorage
		//var localSt = $localStorage;
        //$rootScope.codea_theme = localSt.themas;

		//localSt.themas = vm.theme;
		/* borrar todo el localStorage	
		$localStorage.$reset();*/

		//vm.theme_settings = $rootScope.codea_theme;

       // selecionar tema 
       /*function selectTheme(theme){
       		$log.debug('valor::::', theme);
       		$rootScope.codea_theme = localSt.themas = {
       			'name' : theme.name,
       			'id_theme' : theme.id 
       		};

       		$log.debug($rootScope.codea_theme.name);
       }*/

       /*function defaultTheme () {
       		localSt.themas = null;
       		vm.theme_settings = null;
       }*/

    }
})();
