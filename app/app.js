(function () {
  'use strict';

        angular.module('app', [
          'ngMaterial',
          'ngAnimate',
          'ui.router',
          'ngStorage',
          'app.dashboard',
          'app.authentication',
        ]);
      angular.module('app.authentication', []);
      angular.module('app.dashboard', [
        'dashboard.notas',
        'dashboard.users',
      ]);
      angular.module('dashboard.notas', []);
      angular.module('dashboard.users', []);
})();
