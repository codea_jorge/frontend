(function () { 
    'use strict';
        
    angular
        .module('app')
        .constant('Constants', {

            Themes : [ {
                    'id': 1,
                    'name': 'default',
                    'desc': 'tema por defecto',
                    'autor': 'Jorge Luis Calleja Alvarado'
                },

                {
                    'id': 2,
                    'name': 'django',
                    'desc': 'tema django',
                    'autor': 'Jorge Luis Calleja Alvarado'

                }
            ]

        });

})();
