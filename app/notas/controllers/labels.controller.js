(function () {
    'use strict';

    angular
        .module('dashboard.notas')
        .controller('labelController', labelController);


    function labelController($rootScope, $log, $mdDialog, $localStorage, Constants, $mdToast) {

        var vm = this;
        $log.debug('Controlador de labels');

        // funciones (metodos)
        vm.addLabel = addLabel;
        vm.cancel = cancel;

        function addLabel (answer) {
            $log.debug('datos a insertar:::::', vm.label);
            answer = vm.label;

            $mdToast.show(
              $mdToast.simple()
                .textContent('Etiqueta' + ' ' +  vm.label.title + ' ' + 'creadoa exitosamente')
                .position('top right' ) 
                .hideDelay(3000)
            );

            $mdDialog.hide(answer);
        }

        function cancel () {
            $mdDialog.cancel();
        }

    }
})();
